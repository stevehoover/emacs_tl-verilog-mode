This repository contains:
  - tl-verilog-mode.el: A TL-Verilog mode for Emacs.  TL-Verilog docs can be
                        found at tl-x.org and redwoodeda.com.
  - verilog-mode.el: A copy of the Emacs Verilog mode upon which the TL-Verilog
                     mode is based.  The main download sites for Verilog mode are
                     verilog.com and veripool.org.
